function validate() {
    var month = document.getElementById("txt-month").value * 1;
    var year = document.getElementById("txt-year").value * 1;

    var msgError = "Không xác định"
    var msg = ""
    if (month == 0 && year == 0) {
        msg = msgError;
    }
    document.getElementById("result").innerHTML = msg;
    result.style.background = "#00FF99"
    return msg == "" // true or false
}

function dayCalculation() {
    var valid = validate();
    if (!valid) return false

    var month = document.getElementById("txt-month").value * 1;
    var year = document.getElementById("txt-year").value * 1;

    var dayNumber = 0;
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        dayNumber = 31;
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
        dayNumber = 30;
    } else {
        var checkLeapYear = year % 4 == 0;
        if (checkLeapYear) {
            dayNumber = 29;
        } else {
            dayNumber = 28;
        }
    }
    document.getElementById("result").innerHTML = `Tháng ${month} năm ${year} có ${dayNumber} ngày`;
    result.style.background = "#00FF99";
}