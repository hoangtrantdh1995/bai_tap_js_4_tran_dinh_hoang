function greetingProgram() {
    var family = document.getElementById("greeting").value;
    var greetings = familyGreetings(family);

    document.getElementById("result").innerHTML = greetings;
    result.style.background = "#00FFFF"
}
function familyGreetings(name) {
    switch (name) {
        case "0": {
            return getGreeting("Người lạ ơi")
        }
        case "1": {
            return getGreeting("Bố");
        }
        case "2": {
            return getGreeting("Mẹ");
        }
        case "3": {
            return getGreeting("Anh Trai");
        }
        case "4": {
            return getGreeting("Em Gái");
        }
    }
}
function getGreeting(username) {
    return `Xin chào ${username}!`;
}
