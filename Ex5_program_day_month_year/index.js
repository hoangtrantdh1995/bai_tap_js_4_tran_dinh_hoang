function validate() {
    var day = document.getElementById("txt-day").value * 1;
    var month = document.getElementById("txt-month").value * 1;
    var year = document.getElementById("txt-year").value * 1;

    var msgError = "Ngày không xác định"
    var msg = ""
    if (day < 1 || day > 31 || month < 1 || month > 12 || year < 1) {
        msg = msgError
    } else {
        if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) {
            msg = msgError
        } else if (month == 2 && day > 28) {
            msg = msgError
        }
    }
    document.getElementById("result").innerHTML = msg;
    result.style.background = "#00FF99"
    return msg == "" // true or false
}

function yesterday() {
    var valid = validate();
    if (!valid) return false

    var day = document.getElementById("txt-day").value * 1;
    var month = document.getElementById("txt-month").value * 1;
    var year = document.getElementById("txt-year").value * 1;

    if (day == 1) {
        if (month == 1) {
            day = 31;
            month = 12;
            year--;
        } else if (month == 2 || month == 4 || month == 6 || month == 8 || month == 9 || month == 11) {
            month--;
            day = 31;
        } else if (month == 5 || month == 7 || month == 10 || month == 12) {
            month--;
            day = 30;
        } else if (month == 3) {
            month--;
            day = 28;
        }
    } else {
        day--;
    }
    document.getElementById("result").innerHTML = `Ngày hôm qua: ${day}/${month}/${year}`;
    result.style.background = "#00FF99"
}


function tomorrow() {
    var valid = validate();
    if (!valid) return false

    var day = document.getElementById("txt-day").value * 1;
    var month = document.getElementById("txt-month").value * 1;
    var year = document.getElementById("txt-year").value * 1;


    if (day == 31 && month == 12) {
        day = 1;
        month = 1;
        year++;
    } else if (
        (day == 28 && month == 2) ||
        (day == 30 && (month == 4 || month == 6 || month == 9 || month == 11)) ||
        (day == 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10))
    ) {
        day = 1;
        month++;
    } else {
        day++;
    }

    document.getElementById("result").innerHTML = `Ngày mai: ${day}/${month}/${year}`;
    result.style.background = "#00FF99"
}